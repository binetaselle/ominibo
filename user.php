
    <?php require_once './header.php'?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
          <div class="d-flex flex-row justify-content-between">
          <h1 class="h3 mb-2 text-gray-800">Utilisateur (s)</h1>
          <a href="#" class="btn btn-lg mb-4 pl-4 pr-4 rounded-pill btn-danger" id="btn-add" > <i class="fas fa-marker"></i> Ajouter</a>
          
          </div>
          <!-- <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below. For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p> -->

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>Nom</th>
                      <th>Prenom</th>
                      <th>Email</th> 
                      <th>Role</th> 
                      <!-- <th>Age</th> -->
                      
                      <th>Status</th>
                      
                      <th class="text-center">Action</th> 
                    </tr>
                  </thead>
                  <tbody>
                <?php for ($i=0; $i < 3; $i++) :?>

                    <tr>
                      <td>Dia</td>
                      <td>Ousmane</td>
                      <!-- <td>Edinburgh</td> -->
                      <!-- <td>61</td> -->
                      <td>dia@gmail.com</td>
                      <td>Comunity Manager</td>
                      <td style="text-align: center;"><a href="#"><i class="fa fa-eye"></i></a></td>
                      <td style="text-align: center;"><a href="edit_projet.php"> <i class="fas fa-edit" ></i> </a>&nbsp;&nbsp;&nbsp; <a href="#"> <i class="fas fa-trash" style="color: #BB2628;"></i> </a> </td>


                    </tr>
                    <tr>
                <?php endfor;?>
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->


    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <?php require_once './footer.php'?>
