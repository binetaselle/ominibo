<?php require_once './header.php' ?>

<!-- End of Topbar -->

<!-- Begin Page Content -->
<div class="container-fluid">

    <div class="d-flex flex-row justify-content-between">
        <h1 class="h3 mb-2 text-gray-800">Ajouter</h1>
   

    </div>

    <!-- Page Heading -->


    <!-- Content Row -->
    <div class="container">

        <form class="m-2">

            <div class="form-group">
                <label for="exampleFormControlInput1">Email address</label>
                <input type="email" class="form-control rounded-pill" id="exampleFormControlInput1" placeholder="Champs 1">
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">Email address</label>
                <input type="email" class="form-control rounded-pill" id="exampleFormControlInput1" placeholder="Champs 2">
            </div>
            <div class="form-group">
                <label for="exampleFormControlInput1">Email address</label>
                <input type="email" class="form-control rounded-pill" id="exampleFormControlInput1" placeholder="Champs 3">
            </div>
            <div class="form-group">
                <select class="form-control rounded-pill" id="exampleFormControlSelect1">
                    <option>Champs 4</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary  rounded-pill">Enregistrer</button>
            <button type="button" class="btn btn-danger  rounded-pill">Annuler</button>
        </form>

    </div>





</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="login.php">Logout</a>
            </div>
        </div>
    </div>
</div>
<?php require_once './footer.php' ?>