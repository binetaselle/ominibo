<?php require_once './header.php' ?>

<!-- End of Topbar -->

<!-- Begin Page Content -->
<div class="container-fluid">

  <div class="d-flex flex-row justify-content-between">
    <h1 class="h3 mb-2 text-gray-800">Projet</h1>

    <a href="edit_projet.php" class="btn btn-md mb-4 pl-4 pr-4 rounded-pill btn-danger" id="btn-add"> <i class="fas fa-add"></i> Ajouter</a>


  </div>

  <!-- Page Heading -->


  <!-- Content Row -->
  <div class="row">

    <!-- Earnings (Monthly) Card Example -->
    <?php for ($i = 1; $i < 10; $i++) : ?>
      <div class="col-xl-4 col-md-4 mb-4" id="block">
        <div class="card border-left-info shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-info text-Capitalize mb-1">Projet-<?= $i ?> </div>
                <div class="row no-gutters align-items-center">
                  <a href="projet2.php">
                    <div class="col-auto">
                      <small class="bagde bagde-secondary text-muted ">En savoir plus</small>
                    </div>
                  </a>
                  <div class="col">
                    <div class="progress progress-sm mr-2">
                      <div class="progress-bar bg-info" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-auto">
                <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
        </div>
      </div>

    <?php endfor; ?>

    <?php require_once 'pagination.php' ?>


  </div>





</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-primary" href="login.php">Logout</a>
      </div>
    </div>
  </div>
</div>
<?php require_once './footer.php' ?>


</body>

</html>