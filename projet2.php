<?php require './header.php'; ?>

<!-- End of Topbar -->

<!-- Begin Page Content -->
<div class="container-fluid mt-4">
  <!-- Content Row -->
  <div class="d-flex flex-row justify-content-between">
    <h3 class="infosTitle">Informations du projet</h3>
    <div class="dropdown  mb-4">
      <button class="btn btn-outline-danger dropdown-toggle" id="btn-add" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Action
      </button>
      <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
        <button class="dropdown-item" type="button">Editer</button>
        <button class="dropdown-item text-danger" type="button">Supprimer</button>
      </div>
    </div>
  </div>
  <div class="row infos " style=" margin-left: auto; margin-right: auto">

    <!--Debut div1 -->

    <!--Fin div1 -->

    <!--Debut div2 -->
    <div class="col-xs-12 col-sm-12 col-md-6 ">

      <div class="card shadow mb-6">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <div class="infos_projet">

          </div>
        </div>
        <div class="card-body">
          <ul>
            <li>Type de chantier: </a>
            <li>Nom du chantier: </a>
            <li>Localisation: </a>
            <li>Superficie: </a>
          </ul>
        </div>
      </div>
    </div>
    <!--Fin div2 -->

    <!--Debut div3 -->
    <div class="col-xs-12 col-sm-12 col-md-6">
      <div class="card shadow mb-6">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
          <div class="infos_projet">

          </div>
        </div>
        <div class="card-body">
          <ul>
            <li>Cout du projet: </a>
            <li>Date de debut: </a>
            <li>Date de livraison: </a>
            <li>Commentaire: </a>
          </ul>
        </div>
      </div>
    </div>
    <!--Fin div3 -->

    <!--Debut div3 -->

    <!--Fin div3 -->

  </div>
</div>

<!-- Content Row -->
<div class="container mt-4">
  <div>
    <h3 class="infosTitle">Etat avancement du projet</h3>
  </div>
  <div class="row infos" style=" margin-left: auto; margin-right: auto">
    <div class="col-xs-12 col-md-12 col-sm-12">
      <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->

        <!-- Card Body -->
        <div class="card-body">
          <div class="chart-pie pt-4 pb-2">
            <canvas id="myPieChart"></canvas>
          </div>
          <div class="mt-4 text-center small">
            <span class="mr-2">
              <i class="fas fa-circle text-primary"></i>Pourcentage Réalisé
            </span>
            <span class="mr-2">
              <i class="fas fa-circle text-success"></i>Pourcentage En cours
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
<!-- Footer -->

<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
      <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <a class="btn btn-primary" href="login.html">Logout</a>
      </div>
    </div>
  </div>
</div>
<?php require_once './footer.php' ?>
</body>

</html>